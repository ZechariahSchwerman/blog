---
layout: post
title:  "Bitbucket Pipelines"
date:   2017-10-10 21:14:18 +0000
categories: jekyll update
---

Bitbucket pipelines are away to automatically execute tasks on a repository when code is committed.

Bitbucket pipelines are intended to help with continuous integration, delivery, or deployment.

This post starts with an overview of pipelines, and then shows a simple example that automatically runs tests on a commit.

To use pipelines, they must be enabled for the repository and a valid bitbucket-pipelines.yml must be in the root of the repository.

With pipelines set up correctly, a commit will trigger actions on the source code based on the configuration in bitbucket-pipelines.yml.

The actions are in the script section of bitbucket-pipelines.yml. They execute in a docker image. The docker image runs on a cluster. Notifications and status information from the actions are sent back to the bitbucket web interface. All of this complexity is managed by bitbucket. The user is only has to provide the .yml configuration file.

Currently only linux docker images are supported. The default docker image is Ubuntu 14.04 with some common utilities. Images from dockerhub are usable as are images from a user controlled docker registry.

The actions in the script section are executed like a bash script. Minor alterations occur before the script is executed, such as printfs being added after each command to show status.

Since the script section is basically a bash script, any task can be automated. Typical tasks are building, running tests, and deploying.

An easy first use for pipelines is automatically testing a java maven project.

This example will use eclipse.  The download [eclipse][eclipse-dl]. Run the installer.  Select “Eclipse IDE for Java EE Developers”. Click “Install”. When the install finishes click “Launch”. On the new window that pops up click “Launch” again.

In eclipse, select “File”->”New”->”Other…”. In the window that pops ups scroll down to “Maven” click it and select “Maven Project”. Click “Next” until the window says “Enter a group id for the artifact”. Enter “pipelineTest” for both the Group Id and Artifact Id. Then click “Finish”.

Eclipse will add the maven project to the workspace. Next step is placing the project in a git repository. Right click on the project folder and select “Team->”Share Project” Click the “Create…” button for the Repository directory enter “C:\Users\yourHomeDirectory\git\pipeLineTest” and select “Finish”. Then click “Finish” on the main window.

This created a parent directory which holds the eclipse project in a folder and is the root of the git repository.

Log into your bitbucket account and create a repository named “pipelineTest”. Copy the URL (URL in a box in the top right of the page) to it somewhere. It will be used later.

Go back to eclipse and press ctrl+# to open the commit view. Add a message like “initial commit” and drag all unstaged changes to staged. Click “Commit and Push…”. Paste the bitbucket repository link into the URI box, and everything else needed will autofill. Click “Next” until a window pops up asking for your bitbucket password. Enter your password and click “OK”. Then click “finish” on the main window. Enter your password again and click “OK” then click “OK” on the final window.

Go back to the bitbucket repo and select “Pipelines” on the left sidebar. Scroll down and select “Java (Maven)” and edit the mvn command to

mvn -f pipelineTest/pom.xml -B verify

Click the “Commit file” button.

Your pipeline should run and succeed! This is only the start of bitbucket pipelines. Advanced uses can setup entire testing environments and services they depend on, deploy software to amazon, and more.

[eclipse-dl]: https://www.eclipse.org/downloads/download.php?file=/oomph/epp/oxygen/R/eclipse-inst-win64.exe