---
layout: post
title:  "Phases of Maven"
date:   2017-10-10 21:14:18 +0000
categories: jekyll update
---

##Introduction

Apache Maven is a build tool and a dependency manager made with java and mostly used for java projects.  One of Maven’s overall goals is to force a standard structure onto projects.  In addition, many projects work with the default or nearly default configuration of Maven.  Because of these two facts, understanding a basic Maven project teaches you about Maven projects in general.

Maven has three layers for grouping tasks.  A lifecycle contains phases, a phase contains plugin goals, and plugin goals are the lowest layer and bind to tasks.  A lifecycle defines a high-level task like building and deploying.  Phases break a lifecycle into stages.  Plugin goals connect to actual java code that executes a specific task.  Maven has three built in lifecycles: clean, default, and site.  Clean is intended to put a project in a working state after a failed build attempt.  Default is for building and deploying.  Site reads information from the project and produces files suitable for a documentation website.  While defining custom lifecycles is possible, most projects will not need to.  Customization is typically done by adding new plugin goals to phases in the built-in lifecycles.  To get started with Maven you must learn the phases of the built-in lifecycles.

##Install Maven And Create Project

The easiest way to install Maven on Windows is through chocolatey.  [Install][choco-install] chocolatey and then in an admin Powershell run:

choco install maven

After maven installs open a normal Powershell.  Maven can generate sample projects.  Run this command to create a basic Hello World application.  It will make a new directory named “my-app” and create the project in it.

mvn archetype:generate -D”groupId=com.mycompany.app” -D”artifactId=my-app” -DarchetypeArtifactId=”maven-archetype-quickstart” -DinteractiveMode=”false”

Navigate Powershell into the “my-app” directory.

cd my-app

The Maven command line tool is mvn.  mvn can execute phases by adding the phase name as a command line argument.  mvn requires you to be in a Maven project directory or to tell mvn where a maven project is with a command line argument.  If you are not in a project directory and do not tell maven where one is located, mvn will complain about a missing POM file.  POM stands for Project Object Model.  Pom.xml is the main configuration file for a Maven project.  A project pom inherits configuration from pom files above it.  The root of the pom hierarchy is called the super pom, and it is located in the Maven installation.  This provides default configuration settings.

To verify everything worked run

mvn package

then

java -cp target/my-app-1.0-SNAPSHOT.jar com.mycompany.app.App

This should output “Hello World!”.

##Phases

Now let’s look at the phases of the maven lifecycles.  Executing a phase will also execute all phases leading up to it.  This makes sense, because you could not deploy without first compiling for example.  Some phases are intermediate, and not intended to be executed from the command line.  These typically have a prefix like pre, post, or process.  I will only cover the main phases.

###Clean Lifecycle And Phase

The clean lifecycle has one main phase, which is named clean.

mvn clean

The clean phase deletes the “target” directory in the project.  This is useful because if a build fails in an intermediate state the quickest way to get back to a known working state is to remove the build files.  If the “target” directory does not exist because the project was never built, clean does nothing.

###Default Lifecycle

The default lifecycle has 7 main phases.

####1. Validate

mvn validate

The validate phase checks the project structure, POM, and other files for validity.  This ensures other phases do not try to work with an invalid project.

####2. Compile

mvn compile

The compile phase builds the class files for application code, but not test code.  The class files go into “target/classes/directories/matching/packageName/className.class”. For the sample project the compile phase creates “target/classes/com/mycompany/app/App.class”.  Logs of created or accessed files are made in “target/maven-status/maven-compiler-plugin/compile/default-compile” and named “createdFiles.lst” and “inputFiles.lst”.  Note the compile phase does not make a .jar file.  That is done in the package phase.

####3. Test

mvn test

The test phase builds the test code into class files, then it runs the tests against the application code class files.  The test class files go into “/target/test-classes/directories/matching/packageName/className.class”.  For the sample project this is “target/test-classes/com1/mycompany/app/AppTest.class”.  Like the compile phase, logs of created or accessed files are made in “target/maven-status/maven-compiler-plugin/testCompile/test-compile” and named “createdFiles.lst” and “inputFiles.lst”.

####4. Package

mvn package

The package phase packages the application class files.  The package could be a .jar or another format like .war.  The sample project creates the file “/target/my-app-1.0-SNAPSHOT.jar”.

####5. Verify

mvn verify

The verify phase runs integration tests against the packaged application.

####6. Install

mvn install

The install phase installs the packaged application to the local Maven repository.  Maven repositories are how dependencies are managed for projects.  Built projects are installed to the local repository and required dependencies are downloaded and installed to the local repository.  The local repository location may vary, but if you are following my instructions on Windows the repository will be in “C:/Users/yourUser/.m2/repository”.  For the sample project the .jar file and the project pom file are installed to “/repository/com/mycompany/app/my-app/1.0-SNAPSHOT/my-app-1.0-SNAPSHOT.jar” and “/repository/com/mycompany/app/my-app/1.0-SNAPSHOT/my-app-1.0-SNAPSHOT.pom”.

####7. Deploy

mvn deploy

The deploy phase is similar to the install phase.  The difference is a remote maven repository is used.  The sample project does not have a remote repo configured for deploying to, and will give an error.

 

###Site Lifecycle And Phase

The site lifecycle is short like the clean lifecycle. It has one main phase, which is also named site.

mvn site

The site phase builds html and other files that could be used for a documentation website.  The site files have information about the project such as the dependencies and plugins used.  The files are created in “/target/site”.  The sample project produces a basic documentation site.

 

Maven takes time to learn.  Any tool does.  Compiling, testing, and deploying by hand may initially seem easier, but using a tool becomes more efficient very quickly.  I have very little experience with Maven, but already just adapting the sample project for my projects has saved me development time.

 

Sources:

[https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html][lifecycle]
[https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html][5min]

[5min]: https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html
[lifecycle]: https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html

[choco-install]: https://chocolatey.org/install