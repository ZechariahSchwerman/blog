---
layout: post
title:  "Phases of Maven"
date:   2017-10-10 21:14:18 +0000
categories: jekyll update
---


This post is part 2 of my series about how source code is turned into a running process.
In this post we will install tools for building and analyzing object files and executables.
The tools we will use are the GNU C Compiler (GCC) and utilities from the GNU Compiler Collection (also shortened to GCC).
GCC is aimed at Unix-like operating systems, but a few ways exist to run it on Windows.
I could simply tell you basic instructions for installing GCC on Windows, but that would leave you extremely confused if you ever search for information while troubleshooting.
Instead, first I explain what GCC does and explain different ways to make Windows executables with it.  GCC installation instructions are at the end.